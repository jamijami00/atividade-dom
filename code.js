const $ = document;
const button = document.querySelector("#buttonEnviar");
const list = $.querySelector("#corpoMensagens");

button.addEventListener("click",(event) => {
    
    let filho = $.createElement("div");
    let msg = $.createElement("p");
    let buttonDelete = $.createElement("input");

    buttonDelete.type = "button";
    buttonDelete.value = "Deletar";
    buttonDelete.className = "Deletar"
    buttonDelete.setAttribute("onclick", "deletar(this.parentNode)");

    msg.className = "mensagem";
    msg.innerText = $.getElementById("textareabox").value;
    $.getElementById("textareabox").value = "";

    filho.appendChild(msg);
    filho.appendChild(buttonDelete);

    list.appendChild(filho);
});

function deletar(value){
    value.remove();
}
